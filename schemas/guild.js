var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var guildSchema = new Schema({
  name: String,
  id: String,
  prefix: { type: String, default: "!" },
  date: { type: Date, default: Date.now },
  photoOnly: { type: [String], default: [] },
  musicQueue: { type: [String], default: [] },
  musicPlaying: { type: Boolean, default: false },
  nowPlaying: { type: String, default: null }
});

var Guild = mongoose.model("Guild", guildSchema);
module.exports = Guild;
