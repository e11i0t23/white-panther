const Discord = require("discord.js");

module.exports.noPerms = (message, permission) => {
  let Response = new Discord.RichEmbed()
  .setAuthor(message.author.username)
  .addField("Missing the following permissions: ", permission)
  .setColor("#ed3d02");
  
  message.channel.send(Response).then(m => m.delete(5000));
  message.delete(message.contents)
}

module.exports.noUserFound = (message) => {
  let Response = new Discord.RichEmbed()
  .setTitle("Could not find that user")
  .setColor("#ed3d02");

  message.channel.send(Response).then(m => m.delete(5000));
  message.delete(message.contents)
  
}

module.exports.runSetup = (message) => {
  let Response = new Discord.RichEmbed()
  .addField("Error: ", "Try running `!setup` first")
  .setColor("#ed3d02");

  message.channel.send(Response).then(m => m.delete(5000));
  message.delete(message.contents);

}

module.exports.invalidChannel = (message) => {
  let Response = new Discord.RichEmbed()
  .addField("Error: ", "You can't do that here")
  .setColor("#ed3d02");

  message.channel.send(Response).then(m => m.delete(5000));
  message.delete(message.contents);

}