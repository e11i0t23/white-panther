const { google } = require("googleapis");

module.exports = client => {
  client.getGuild = async guild => {
    let data = await client.Guild.findOne({ id: guild.id });
    if (data) return data;
  };

  client.ytsearch = async term => {
    console.log("ytsearch");
    var service = google.youtube({
      version: "v3",
      auth: process.env.YOUTUBE_API
    });
    const s = await service.search.list({
      part: "snippet",
      q: term
    });
    console.log(s.data.items);
    return s.data.items;
  };
};
