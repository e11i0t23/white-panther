const { prefix, token } = require("./config.json");
//imports the prefix currently "!" and the discord token
const Discord = require("discord.js"); //discord libary for javascripts
const fs = require("fs"); //node.js file system

const client = new Discord.Client();
const newUsers = [];
require("dotenv").config();
require("./utils/funtions")(client);
client.Guild = require("./schemas/guild");
client.photochannel = "444687905147977738"; //channel id for #photos_only
client.commands = new Discord.Collection();
client.mongoose = require("./utils/db");

fs.readdir("./commands/", (err, files) => {
  if (err) console.log(err);
  let jsfile = files.filter(f => f.split(".").pop() === "js");
  if (jsfile.length <= 0) {
    console.log("Couldn't find commands.");
    return;
  }
  jsfile.forEach((f, i) => {
    let props = require(`./commands/${f}`);
    console.log(`${f} loaded!`);
    client.commands.set(props.help.name, props);
  });
});

client.on("ready", () => {
  client.user.setActivity("The World", { type: "WATCHING" });
  console.log(
    `${client.user.username} is now online on ${client.guilds.size} servers`
  );
});

async function main() {
  try {
    await client.mongoose.init();
    client.login(process.env.TOKEN);
  } catch (error) {
    console.log(error);
    main();
  }
}

fs.readdir("./events/", (err, files) => {
  if (err) return console.error;
  files.forEach(file => {
    if (!file.endsWith(".js")) return;
    const evt = require(`./events/${file}`);
    let evtName = file.split(".")[0];
    console.log(`Loaded event '${evtName}'`);
    client.on(evtName, evt.bind(null, client));
  });
});

main();
