# WHITE PANTHER

###### Who is White Panther?

White Panther is a Discord bot made by me (u/e11i0t23#7272 on discord) and acts as an admin bot for my test servers


###### Why make a new bot then?

Well I wanted to learn about node.js as well as make something practicle that i could use. Also gives me the ability to learn discord.js and some other node mudules while a it.

###### What is special about White Panther?

Ummmmm......That I made it, Does that count?

###### How to use:

1) First head to https://nodejs.org/en/ and install the recomended version along with npm.
2) Run git clone https://gitlab.com/e11i0t23/white-panther.git
3) Head to https://discordapp.com/developers/applications/me and create a new app
4) On the new app select "Create a bot user"
5) Add a config.json file to the white-panther folder
6) add the following to your config.json
```javascript
{
    "prefix": "YOUR_PREFIX",
    "token": "YOUR_TOKEN"
}
```

make sure to replace YOUR_PREFIX with whatever prefix you want the bot commands to use,

make sure to replace YOUR_TOKEN with the token from the bot section in your app

7) run the bot with `node index.js`
8) add the bot to your server through the developer app page

###### Profit!!!!