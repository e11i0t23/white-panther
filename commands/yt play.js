const Discord = require("discord.js");
const ytdlDiscord = require("ytdl-core-discord");
const ytdl = require("ytdl-core");

module.exports.run = async (client, message, args) => {
  if (!message.member.voiceChannel)
    return message.reply("You need to join a vioce channel first");

  const url = await handleUrl(client, message, args);
  console.log(url);
  if (url == null) return null;
  await client.Guild.updateOne(
    { id: message.guild.id },
    { $push: { musicQueue: url } }
  ).exec();
  data = await client.getGuild(message.guild);
  musicQueue = data.musicQueue;
  if (data.musicPlaying) {
    Response = new Discord.RichEmbed()
      .setTitle("Que")
      .setColor("#ed3d02")
      .addField("Now Playing:", data.nowPlaying);
    length = data.musicQueue.length;
    if (data.musicQueue.length > 5) length = 4;
    for (var i = 1; i < length; i++) {
      const trackInfo = await ytdl.getBasicInfo(musicQueue[i]);
      Response.addField(
        i.toString(),
        Discord.Util.escapeMarkdown(trackInfo.title)
      );
      if (i == length - 1) message.channel.send(Response);
    }
    return null;
  }
  const connection = await message.member.voiceChannel.join();
  const play = async (song, connection) => {
    const trackInfo = await ytdl.getBasicInfo(song);
    const dispatch = connection
      .playOpusStream(await ytdlDiscord(song), { passes: 3 })
      .on("end", reason => {
        if (reason === "Stream is not generating quickly enough.")
          console.log("Song ended.");
        else console.log(reason);
        onEnd();
      })
      .on("error", e => {
        console.log(e);
        onEnd();
      })
      .setVolume(1);
    console.log(`Playing ${song}`);
    console.log(trackInfo);
    client.Guild.updateOne(
      { id: message.guild.id },
      {
        musicPlaying: true,
        nowPlaying: Discord.Util.escapeMarkdown(trackInfo.title)
      }
    ).exec();
  };

  const onEnd = async () => {
    await client.Guild.updateOne(
      { id: message.guild.id },
      { $pop: { musicQueue: -1 } }
    ).exec();
    data = await client.getGuild(message.guild);
    musicQueue = data.musicQueue;
    if (musicQueue.length < 1) {
      client.Guild.updateOne(
        { id: message.guild.id },
        { musicPlaying: false, nowPlaying: null }
      ).exec();
      return message.member.voiceChannel.leave();
    }
    play(musicQueue[0], connection);
  };

  try {
    const connection = await message.member.voiceChannel.join();
    play(musicQueue[0], connection);
  } catch (e) {
    console.log(`I couldnt join voice channel error: ${e}`);
    message.member.voiceChannel.leave();
    message.channel.send(`I couldnt join voice channel error: ${e}`);
  }
};

module.exports.help = {
  name: "ytplay",
  description: "Play a video from youtube in voice channel"
};

async function handleUrl(client, message, args) {
  let url = await args.join(" ").toString();
  //console.log(url);
  return new Promise(async function(resolve, reject) {
    if (url.indexOf("youtube.com") < 0) {
      const videoSearch = await client.ytsearch(url);
      Response = new Discord.RichEmbed().setTitle("Choices");
      for (let i = 0; i < videoSearch.length; i++) {
        const vid = videoSearch[i];
        Response.addField((i + 1).toString(), vid.snippet.title);
        if (vid == videoSearch[videoSearch.length - 1]) {
          message.reply(Response).then(r => r.delete(10000));
          message
            .reply("Which Video would you like? Expires in 10s")
            .then(r => r.delete(10000));
        }
      }
      const filter = m => m.author.id === message.author.id;
      message.channel.awaitMessages(filter, { max: 1, time: 10000 }).then(m => {
        res = m.first().content;
        if (res.match(/[1-5]/)) {
          url = `https://youtube.com/watch?v=${
            videoSearch[parseInt(res, 10) - 1].id.videoId
          }`;
          //console.log(url);
          m.first().delete();
          resolve(url);
        } else {
          m.first().delete();
          resolve(null);
        }
      });
    } else {
      resolve(url);
    }
  });
}
