const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
  commands = Array.from(client.commands.values()).sort();
  Response = new Discord.RichEmbed()
    .setTitle("Available Commands:")
    .setColor("#ed3d02");
  console.log(commands);
  counter = commands.length;
  commands.forEach(command => {
    help = command.help;
    if (help.permissions != null) {
      if (message.member.hasPermission(help.permission)) {
        Response.addField(help.name, help.description);
      }
    } else {
      Response.addField(help.name, help.description);
    }

    counter -= 1;
    if (counter === 0) {
      message.channel.send(Response);
    }
  });
};

module.exports.help = {
  name: "help",
  description: "Shows a list of available commands"
};
