const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
  if (!message.member.hasPermission("MANAGE_GUILD"))
    return message.reply("Sorry you dont have permissions for that");
  console.log(args);
  client.Guild.updateOne({ id: message.guild.id }, { prefix: args[0] }).exec();
  message.channel.send(`Prefix Changed to ${args[0]}`);
};

module.exports.help = {
  name: "prefix",
  description: "Change the bot prefix",
  permission: "MANAGE_GUILD"
};
