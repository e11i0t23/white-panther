const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
  data = await client.getGuild(message.guild);
  if (!data.musicPlaying) message.channel.send("no music playing");
  message.channel.send(`currently playing ${data.nowPlaying}`);
};

module.exports.help = {
  name: "ytnp",
  description: "Show information on currently playing track"
};
