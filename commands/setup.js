const Discord = require("discord.js");
const errors = require("../utils/errors.js");
module.exports.run = async (client, message, args) => {
  if (!message.member.hasPermission("ADMINISTRATOR"))
    return errors.noPerms(message, "ADMINISTRATOR");

  //DEPRICATING
  return null;

  let server = message.guild;
  console.log("Running setup script on: " + server.name);
  message.channel.send("Running Setup script").then(m => m.delete(5000));

  message.channel.send("Cretaing new member role").then(m => m.delete(5000));
  let memberRole = message.guild.roles.find(`name`, "Unauthorised");
  if (!memberRole) {
    message.guild.createRole({
      name: "Unauthorised",
      color: "#000000",
      permissions: []
    });
  }

  message.channel.send("Creating reports channel").then(m => m.delete(5000));
  let findReports = message.guild.channels.find(`name`, "reports");
  if (!findReports) {
    message.guild.createChannel("reports", "Text");
    message.channel.send("Created channel").then(m => m.delete(5000));
  }

  message.channel
    .send("Creating welcome rules channel")
    .then(m => m.delete(5000));
  let findWelcome = message.guild.channels.find(`name`, "welcome-rules");
  if (!findWelcome) {
    message.guild.createChannel("welcome-rules", "Text");
    message.channel.send("Created channel").then(m => m.delete(5000));
  }

  message.channel
    .send("Creating notification channel")
    .then(m => m.delete(5000));
  let findNotifications = message.guild.channels.find(`name`, "notifications");
  if (!findNotifications) {
    message.guild.createChannel("notifications", "Text");
    message.channel.send("Created channel").then(m => m.delete(5000));
    let chtest = message.guild.channels.find(`name`, "notifications");
    if (!chtest) return errors.runSetup(message);
  }
  message.channel.send("Configuring permissions").then(m => m.delete(5000));

  message.guild.channels.forEach(async (channel, id) => {
    await channel.overwritePermissions(
      message.guild.roles.find(`name`, "@everyone"),
      {
        SEND_MESSAGES: null,
        READ_MESSAGES: null,
        ADD_REACTIONS: null,
        CONNECT: null,
        READ: null
      }
    );
  });

  let notifcations = message.guild.channels.find(`name`, "notifications");
  notifcations.overwritePermissions(
    message.guild.roles.find(`name`, "@everyone"),
    {
      SEND_MESSAGES: false,
      READ_MESSAGES: false
    }
  );

  let welcome = message.guild.channels.find(`name`, "welcome-rules");
  welcome.overwritePermissions(message.guild.roles.find(`name`, "@everyone"), {
    SEND_MESSAGES: false
  });

  // welcome.overwritePermissions(message.guild.roles.find(`name`, "Unautherised"),{
  //     SEND_MESSAGES: true,
  //     READ_MESSAGES: true
  // });

  welcome.overwritePermissions(message.guild.roles.find(`name`, "@everyone"), {
    READ_MESSAGES: true,
    SEND_MESSAGES: true
  });

  message.channel.send("Configed").then(m => m.delete(5000));

  message.channel.send("Creating Mute role").then(m => m.delete(5000));
  let muterole = message.guild.roles.find(`name`, "muted");
  if (!muterole) {
    try {
      muterole = await message.guild.createRole({
        name: "muted",
        color: "#000000",
        permissions: []
      });
      message.guild.channels.forEach(async (channel, id) => {
        await channel.overwritePermissions(muterole, {
          SEND_MESSAGES: false,
          READ_MESSAGES: false,
          ADD_REACTIONS: false,
          CONNECT: false,
          READ: false
        });
      });
      message.channel.send("Created").then(m => m.delete(5000));
    } catch (e) {
      console.log(e.stack);
      messge.channel
        .send("An error has occured try running again")
        .then(m => m.delete(5000));
    }
  }

  message.channel.send("Automatic setup complete").then(m => m.delete(5000));
  message.channel
    .send(
      "Please manualy add rules to welcome server remembering to include `!accept` to tell users to accept the rules"
    )
    .then(m => m.delete(5000));

  console.log("Finnished");
  message.delete(message.contents);
};

module.exports.help = {
  name: "setup",
  description: "Setup command(depricated)",
  permission: "ADMINISTRATOR"
};
