const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
  let sicon = message.guild.iconURL;
  let serverembeded = new Discord.RichEmbed()
    .setTitle("server info")
    .setColor("#d4ef09")
    .setThumbnail(sicon)
    .addField("Server Name:", message.guild.name)
    .addField("Created on:", message.guild.createdAt)
    .addField("Total members:", message.guild.memberCount)
    .addField("You joined:", message.member.joinedAt);
  message.channel.send(serverembeded);
  console.log("___Server info displayed___");
};

module.exports.help = {
  name: "sinfo",
  description: "Lists some information about the server"
};
