const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
  data = await client.getGuild(message.guild);
  if (!data.musicPlaying) message.channel.send("no music playing");
  Response = new Discord.RichEmbed()
    .setTitle("Que")
    .setColor("#ed3d02")
    .addField("Now Playing:", data.nowPlaying);
  length = data.musicQueue.length;
  if (data.musicQueue.length > 5) length = 4;
  for (var i = 1; i < length; i++) {
    const trackInfo = await ytdl.getBasicInfo(musicQueue[i]);
    Response.addField(
      i.toString(),
      Discord.Util.escapeMarkdown(trackInfo.title)
    );
    if (i == length - 1) message.channel.send(Response);
  }
};

module.exports.help = {
  name: "ytq",
  description: "Show information on track queue"
};
