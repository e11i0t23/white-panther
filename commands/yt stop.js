const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
  message.member.voiceChannel.leave();
  console.log("__music stopped_");
  client.Guild.updateOne(
    { id: message.guild.id },
    { musicPlaying: false, nowPlaying: null }
  ).exec();
};

module.exports.help = {
  name: "ytstop",
  description: "Stop a video from youtube"
};
