const Discord = require("discord.js");
const errors = require("../utils/errors.js");

module.exports.run = async (client, message, args) => {
  let welcomeChannel = message.guild.channels.find(`name`, "welcome-rules");
  if (!welcomeChannel) return errors.runSetup(message);
  if (welcomeChannel.id != message.channel.id)
    return errors.invalidChannel(message);

  message.member.removeRole(
    message.guild.roles.find(`name`, "Unauthorised").id
  );
  message.delete(message.content);
  console.log("accepted rules");
};

module.exports.help = {
  name: "accept",
  description: "Accept rules(depricated)"
};
