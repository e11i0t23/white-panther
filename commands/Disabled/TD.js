const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
    //delete message in #photos-only if no attachment is pressent
    if (message.channel.id === photochannel){  //see if message has been posted in #photos-photos_only
            if (message.attachments.size === 0) {  //see if message includes attachments
            //create user DM warning
            let botemebeded_po = new Discord.RichEmbed()
            .setDescription("WARNING: ")
            .setColor("#d0ef04")
            .addField("Violation: ", `posting text in <#${photochannel}>`)
            .addField("Violated by:", `${un} with ID:${message.author.id}`)
            .addField("Violated at", message.createdAt)
            .setFooter(`This bot was custom made by u/e11i0t23#7272 on ${client.user.createdAt}`);
            //send message in dm
            message.author.send(botemebeded_po);
            //delete there original message
            message.delete(message.contents);
            //send log to console
            console.log('---------------------Message Deleted---------------------')
            console.log(`__user ${un} posted text in photos only__`)
        }
    }
}

module.exports.help = {
  name: ""
}
