const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
  if (message.member.hasPermission("BAN_MEMBERS")) {
    const taggedUser =
      message.mentions.members.first() || message.guild.members.get(args[0]);
    if (!taggedUser) return message.channel.send("Invalid user selected");
    else if (!taggedUser.hasPermission("BAN_MEMBERS")) {
      taggedUser.ban();
      let rreason = args.join(" ").slice(22);
      let banned = new Discord.RichEmbed()
        .setColor("#700113")
        .addField("Banned user:", taggedUser)
        .addField("Banned from:", message.guild.name)
        .addField("Banned at:", message.createdAt)
        .addField("Banned by:", message.author)
        .addField("Banned for:", rreason);
      let notificationschannel = message.guild.channels.find(
        `name`,
        "notifications"
      );
      if (!notificationschannel)
        return message.channel.send("Couldn't find notifications channel.");
      notificationschannel.send(banned);
      console.log("__member banned__");
    } else
      return (
        message.channel.send("You need higher privileges"),
        message.delete(message.content),
        console.log("__Ban attempted__")
      );
  } else
    return (
      message.channel.send("You don't have permission for that"),
      message.delete(message.content),
      console.log("__Ban attempted__")
    );
};

module.exports.help = {
  name: "permaban",
  description: "Ban a user",
  permission: "BAN_MEMBERS"
};
