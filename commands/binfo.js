const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
  let botembeded_po = new Discord.RichEmbed()
    .setTitle("Bot info:")
    .setDescription("created by u/e11i0t23")
    .setColor("#d0ef07")
    .setThumbnail(client.user.displayAvatarURL)
    .addField("Bot name", client.user.username)
    .addField("Created on:", client.user.createdAt);
  message.channel.send(botembeded_po);
  console.log("__Bot info posted__");
};

module.exports.help = {
  name: "binfo",
  description: "List basic info about this bot"
};
