const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
  //!report @ned this is the reason
  let rUser = message.guild.member(
    message.mentions.users.first() || message.guild.members.get(args[0])
  );
  if (!rUser) return message.channel.send("Couldn't find user.");
  let rreason = args.join(" ").slice(22);
  var cfind = message.guild.channels.find(`name`, "reports");
  if (!cfind) {
    message.guild.createChannel("reports", "Text");
  }
  let reportschannel = message.guild.channels.find(`name`, "reports");
  let reportEmbed = new Discord.RichEmbed()
    .setDescription("Reports")
    .setColor("#15f153")
    .addField("Reported User", `${rUser} with ID: ${rUser.id}`)
    .addField("Reported By", `${message.author} with ID: ${message.author.id}`)
    .addField("Channel", message.channel)
    .addField("Time", message.createdAt)
    .addField("Reason", rreason);

  if (!reportschannel)
    return message.channel.send("error").then(m => m.delete(3000));
  message.delete().catch(O_o => {});
  reportschannel.send(reportEmbed);
  console.log(`__ user ${rUser} has been reported__`);
};

module.exports.help = {
  name: "report",
  description: "Report another user"
};
