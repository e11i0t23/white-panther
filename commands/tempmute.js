const Discord = require("discord.js");
const ms = require("ms");
const errors = require("../utils/errors.js");

module.exports.run = async (bot, message, args) => {
  if (!message.member.hasPermission("BAN_MEMBERS"))
    return errors.noPerms(message, "BAN_MEMBERS");
  //!tempmute @user 1s/m/h/d
  let tomute = message.guild.member(message.mentions.users.first());
  if (!tomute) return errors.noUserFound(message);
  if (
    tomute.hasPermission("MANAGE_MESSAGES") &&
    !message.member.hasPermission("ADMINISTRATOR")
  )
    return errors.noPerms(message, "MUTE_STAFF");
  let muterole = message.guild.roles.find(`name`, "muted");
  //start of create role
  if (!muterole) return errors.runSetup(message);
  //end of create role
  let mutetime = args[1];
  if (!mutetime) return message.reply("You didn't specify a time!");

  if (!message.member.hasPermission("MANAGE_MESSAGES"))
    return message.reply("Sorry you dont have permissions for that");
  await tomute.addRole(muterole.id);
  //message.reply(`<@${tomute.id}> has been muted for ${ms(ms(mutetime))}`);
  let mutedembeded = new Discord.RichEmbed()
    .setDescription("Warning:")
    .setColor("#700113")
    .addField("Violated by", `<@${tomute.id}>`)
    .addField("Violated at:", message.createdAt)
    .addField("Banned for:", mutetime + "secs")
    .addField("Banned by", `<@${message.author.id}>`);
  tomute.send(mutedembeded);
  let notificationschannel = message.guild.channels.find(
    `name`,
    "notifications"
  );
  if (!notificationschannel)
    return message.channel.send("Couldn't find notifications channel.");
  notificationschannel.send(mutedembeded);

  setTimeout(function() {
    tomute.removeRole(muterole.id);
    //message.channel.send(`<@${tomute.id}> has been unmuted!`);
    let unmutedembeded = new Discord.RichEmbed()
      .addField("Ban Over for:", `<@${tomute.id}>`)
      .addField("Violated at:", message.createdAt)
      .addField("Banned for:", mutetime)
      .setColor("#d0ef04");
    notificationschannel.send(unmutedembeded);
  }, ms(mutetime));

  //end of module
};

module.exports.help = {
  name: "tempban",
  description: "temporarily ban a user",
  permission: "BAN_MEMBERS"
};
