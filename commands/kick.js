const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
  if (!message.member.hasPermission("KICK_MEMBERS"))
    return message.reply("Sorry you dont have permissions for that");
  const taggedUser = message.guild.member(
    message.mentions.users.first() || message.guild.members.get(args[0])
  );
  if (!taggedUser) return message.channel.send("Invalid user selected");
  if (taggedUser.hasPermission("KICK_MEMBERS"))
    return message.reply("Sorry you cant kick this usser");
  let rreason = args.join(" ").slice(22);
  //message.channel.send(`${taggedUser} successfully kicked`);
  let kick = new Discord.RichEmbed()
    .setColor("#700113")
    .addField("Kicked user:", taggedUser)
    .addField("Kicked from:", message.guild.name)
    .addField("Kicked at:", message.createdAt)
    .addField("Kicked by:", message.author);
  try {
    kick.addField("Kicked for:", rreason);
  } catch (e) {
    console.log("__an error occured__");
  }
  let notificationschannel = message.guild.channels.find(
    `name`,
    "notifications"
  );
  if (!notificationschannel)
    return message.channel.send("Couldn't find notifications channel.");
  notificationschannel.send(kick);
  taggedUser.send(kick);
  console.log("_member kicked__");
  taggedUser.kick();
};

module.exports.help = {
  name: "kick",
  description: "Kick a member from the server",
  permission: "KICK_MEMBERS"
};
