const Discord = require("discord.js");

module.exports.run = async (client, message, args) => {
  if (!message.member.hasPermission("MANAGE_CHANNELS"))
    return errors.noPerms(message, "MANAGE_CHANNELS");
  const data = await client.getGuild(message.guild);
  console.log(`data: ${data}`);
  let photochannels = data.photoOnly;
  if (photochannels.includes(message.channel.id)) {
    await client.Guild.updateOne(
      { id: message.guild.id },
      { $pull: { photoOnly: message.channel.id } }
    ).exec();
    res = "removed from photoOnly";
    console.log;
  } else {
    await client.Guild.updateOne(
      { id: message.guild.id },
      { $push: { photoOnly: message.channel.id } }
    ).exec();
    res = "set as photoOnly";
  }
  message.delete(message.content);
  message.channel.send(res);
};

module.exports.help = {
  name: "setphoto",
  description: "Toggle channel as photo only",
  permission: "MANAGE_CHANNELS"
};
