const Discord = require("discord.js");
module.exports = async (client, message) => {
  try {
    if (message.member.id == process.env.Botid) return null;
    console.log(
      `new message from "@${message.member.displayName}" in "#${
        message.channel.name
      }" on "${message.guild.name}" saying "${message.content}"`
    );

    //console.log(message.guild);
    var settings = await client.getGuild(message.guild);
    //console.log(settings);
    prefix = settings.prefix;
    // define varibles

    //let checkrole = message.mentions.members.first().roles.some(r=>["Mod", "Server Staff", "Supreme master leader", "White Panther"].includes(r.name));
    let messageArray = message.content.split(" ");
    let cmd = messageArray[0];
    let args = messageArray.slice(1);
    let un = `<@${message.author.id}>`;

    if (message.content[0] == prefix) {
      let commandfile = client.commands.get(cmd.slice(prefix.length));

      if (commandfile) commandfile.run(client, message, args);
    }

    //const checkprivleges = message.member.roles.some(r=>["Mod", "Server Staff", "Supreme master leader", "White Panther"].includes(r.name));

    //delete message in #photos-only if no attachment is pressent
    if (settings.photoOnly.includes(message.channel.id)) {
      //see if message has been posted in #photos-photos_only
      if (message.attachments.size === 0) {
        //see if message includes attachments
        //create user DM warning
        let botemebeded_po = new Discord.RichEmbed()
          .setDescription("WARNING: ")
          .setColor("#d0ef04")
          .addField("Violation: ", `posting text in <#${message.channel.id}>`)
          .addField("Violated by:", `${un} with ID:${message.author.id}`)
          .addField("Violated at", message.createdAt)
          .setFooter(
            `This bot was custom made by u/e11i0t23#7272 on ${
              client.user.createdAt
            }`
          );
        //send message in dm
        message.author.send(botemebeded_po);
        //delete there original message
        message.delete(message.contents);
        //send log to console
        console.log(
          "---------------------Message Deleted---------------------"
        );
        console.log(`__user ${un} posted text in photos only__`);
      }
    }
  } catch (e) {
    console.log(`an error occured`);
    console.error(e);
  }
};
